from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from application.models import User


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[Length(min=6, max=20)],
                           render_kw={"placeholder": "Enter your username..."})
    email = StringField('Email', validators=[Email()], render_kw={"placeholder": "Enter your email..."})
    password = PasswordField('Password', validators=[Length(min=6, max=10)],
                             render_kw={"placeholder": "Enter your password..."})
    confirm_password = PasswordField('Confirm password', validators=[EqualTo('password')],
                                     render_kw={"placeholder": "Retype your password..."})
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Username already exists in database')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Email already exists in database')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()],
                        render_kw={"placeholder": "Enter your email..."})
    password = PasswordField('Password', validators=[DataRequired(), Length(min=6, max=10)],
                             render_kw={"placeholder": "Enter your password..."})
    submit = SubmitField('Login')
    # remember_me = BooleanField('Remember me')


class UpdateProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=6, max=20)],
                           render_kw={"placeholder": "Enter your username..."})
    email = StringField('Email', validators=[DataRequired(), Email()], render_kw={"placeholder": "Enter your email..."})
    picture = FileField('Change your profile photo', validators=[FileAllowed(['jpg', 'png', 'svg', 'jpeg'])])
    submit = SubmitField('Update Profile')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('Username already exists in database')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('Email already exists in database')


class CreatePost(FlaskForm):
    title = StringField('Title', validators=[Length(min=6, max=50)],
                        render_kw={"placeholder": "Enter post title"})
    content = TextAreaField('Content', validators=[], render_kw={"placeholder": "Enter post body"})
    submit = SubmitField('Create post')


class EditPost(FlaskForm):
    title = StringField('Title', validators=[DataRequired(), Length(min=6, max=50)])
    content = TextAreaField('Content', validators=[DataRequired()])
    submit = SubmitField('Update post')


class ResetForm(FlaskForm):
    email = StringField('Email', validators=[Email()], render_kw={"placeholder": "Enter your email..."})
    submit = SubmitField('Request reset Password')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('There is no account with this email')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[Length(min=6, max=10)],
                             render_kw={"placeholder": "Enter your password..."})
    confirm_password = PasswordField('Confirm password', validators=[EqualTo('password')],
                                     render_kw={"placeholder": "Retype your password..."})
    submit = SubmitField('Reset Password')
