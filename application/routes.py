import secrets
import os
from PIL import Image  # pillow
from flask import render_template, flash, redirect, url_for, request, abort
from application.forms import RegistrationForm, LoginForm, UpdateProfileForm, CreatePost, EditPost, ResetForm, \
    ResetPasswordForm
from application.models import User, Post
from application import app, bcrypt, db, mail
from flask_login import login_user, current_user, logout_user, login_required
from flask_mail import Message

"""" MAIN ROUTES """

@app.route('/')
def home():

    # post = Post.query.get_or_404(post_id)
    posts = Post.query.order_by(Post.date_posted.desc())
    return render_template('index.html', title='Home', posts=posts, post=post,
                           )


@app.route('/blog')
def blog():
    # post = Post.query.all()
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=3)
    return render_template('pages/blog.html', title='Blog', posts=posts)


@app.route('/about')
def about():
    return render_template('pages/about.html', title='About')


@app.route('/authors')
def authors():
    users = User.query.order_by(User.username)
    return render_template('pages/authors.html', title='Authors', users=users)


"""" ROUTES FOR USER LOGIN, REGISTER AND RESET PASSWORD """


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hash_pass = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hash_pass)
        db.session.add(user)
        db.session.commit()
        flash(f'Your account has been created', 'success')
        return redirect(url_for('login'))
    return render_template('user/register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            # login_user(user, remember=form.remember_me.data)
            login_user(user)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            form.email.data = ''
            form.password.data = ''
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('user/login.html', title='Login', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


def save_picture(picture):
    random = secrets.token_hex(8)  # kreira random naziv slike
    name, extension = os.path.splitext(picture.filename)  # deli ime i ekstenziju
    picture_name = random + extension  # spaja hex sa ekstenzijom
    picture_path = os.path.join(app.root_path, 'static/images', picture_name)  # spaja putanju gde ce se nalaziti slika
    picture_size = (200, 200)
    resized_picture = Image.open(picture)
    resized_picture.thumbnail(picture_size)
    resized_picture.save(picture_path)  # snima sliku
    return picture_name


@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    form = UpdateProfileForm()
    if form.validate_on_submit():
        if form.picture.data:
            file_picture = save_picture(form.picture.data)
            current_user.image_file = file_picture
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your account is updated', 'success')
        return redirect(url_for('profile'))
    elif request.method == "GET":
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='images/' + current_user.image_file)
    return render_template('user/profile.html', title='Profile', image_file=image_file, form=form)


def send_email(user):
    token = user.get_reset_token()
    msg = Message('Reset Password', sender='nikolajovic91@hotmail.com', recipients=[user.email])
    msg.body = f'''
        To reset your password, visit the following link: 
{url_for('reset_token', token=token, _external=True)} 
If you did not make this request then ignore this email
    '''
    mail.send(msg)


@app.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = ResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_email(user)
        flash('Email has been sent to your email', 'info')
        return redirect(url_for('login'))
    return render_template('user/reset_request.html', title='Reset password', form=form)


@app.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is invalid or expired token', 'warning')
        return redirect(url_for('reset_request'))

    form = ResetPasswordForm()
    if form.validate_on_submit():
        hash_pass = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hash_pass
        db.session.commit()
        flash(f'Your password has been updated!', 'success')
        return redirect(url_for('login'))
    return render_template('user/reset_token.html', title='Reset password', form=form)


"""" ROUTES FOR CREATING, EDITING AND DELETING POSTS """


@app.route('/user/<string:username>')
def user_posts(username):
    # post = Post.query.all()
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user).order_by(Post.date_posted.desc()).paginate(page=page, per_page=3)
    return render_template('post/user_posts.html', title='User post', posts=posts, user=user)


@app.route('/post/new_post', methods=['GET', 'POST'])
@login_required
def new_post():
    form = CreatePost()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('You created new post', 'success')
        return redirect(url_for('home'))
    image_file = url_for('static', filename='images/' + current_user.image_file)
    return render_template('post/post_add.html', title='Create post', form=form, image_file=image_file)


@app.route('/post/<int:post_id>')
def post(post_id):
    post = Post.query.get_or_404(post_id)  # mozes i samo get metodu
    return render_template('post/post.html', title=post.title, post=post)


@app.route('/post/<int:post_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_post(post_id):
    post = Post.query.get(post_id)
    if post.author != current_user:
        abort(403)
    form = EditPost()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash("Your post has been updated!", 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == "GET":
        form.title.data = post.title
        form.content.data = post.content
    return render_template('post/post_edit.html', title='Edit post', form=form)


@app.route('/post/<int:post_id>/delete', methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash("Your post has been deleted!", 'success')
    return redirect(url_for('home'))


"""" ROUTES FOR ERROR PAGES"""


@app.errorhandler(404)
def page404(e):
    return render_template('errors/page404.html', title='404 page'), 404


@app.errorhandler(403)
def page403(e):
    return render_template('errors/page403.html', title='403 page'), 403


@app.errorhandler(500)
def page500():
    return render_template('errors/page500.html', title='500 page'), 500
