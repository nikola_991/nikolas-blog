console.log('nikola')

setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
},  4000);


const btnCardShowMore = document.getElementById("show-more-btn");
if (btnCardShowMore) {
    btnCardShowMore.addEventListener('click', function () {
        let hiddenText = document.getElementById("hiddenText");
        let showText = document.getElementById("showText");
        if (hiddenText.style.display === "none") {
             window.location.href = '/blog'
        } else {
            hiddenText.style.display = "none";
            showText.style.display = "grid";
            btnCardShowMore.innerHTML = "All posts";
        }
    });
}